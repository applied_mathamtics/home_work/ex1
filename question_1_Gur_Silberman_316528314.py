"""
Name: Gur Silberman
Id: 316528314
"""
import numpy as np

A = np.array([[1,1,0],[1,-2,1],[2,1,2]])
X = np.array([[1],[2],[-2]])
B = np.array([[1,-1,1],[-1,0,1],[3,2,1]])
Y = np.array([[2,-1,3]])
C = np.array([[1,-1j],[-1,1j],[0,2j]])
#A)
try:
 print("---AX----\n")
 print(np.matmul(A,X))
except:
    print("can not do mul AX\n")
#B)	
try:
    print("---BXC---\n")
    print(np.matmul(np.matmul(B,X),C))
except:
    print("can not do mul BXC\n")
#C)
try:
    print("---YAB---\n")
    print(np.matmul(np.matmul(Y,A),B))
except:
    print("can not do mul YAB\n")
#D)
try:
    print("---CC^TX---\n")
    print(np.matmul(np.matmul(C,C.transpose()),X))
except:
    print("can not do mul CC^TX\n")
#E)
try:
 print("---YX----\n")
 print(np.matmul(Y,X))
except:
    print("can not do mul YX\n")

#one way to do it
AB = np.matmul(A,B)
print(AB.sum())

#seconde way to do it
def sumOFMatrix(A,B):
    counter = 0
    AB = np.matmul(A, B)
    for row in range (len(AB)):
        for col in range (len(AB[0])):
            counter+=AB[row][col]
    return counter
print(sumOFMatrix(A,B))

