"""
Name: Gur Silberman
Id: 316528314
"""
import numpy as np

def innerProd(A,B):
	if(len(A) != len(B)):
		return Exception("Wrong size, not have the same length")
	return np.inner(A,B)

#iniliaze varable
U = np.array([2j,3,5,-2,5])
V = np.array([1j,-1j,2,1,5])
W = np.array([2,3,-3j,-1,3])

print(innerProd(U,2*V))
print(innerProd(U,V+2*W))
