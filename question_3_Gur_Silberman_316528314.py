"""
Name: Gur Silberman
Id: 316528314
"""

import numpy as np
import matplotlib.pyplot as plt
from operator import mul

# part one
Ts = 1/44100
X = np.arange(0,1-Ts,Ts)
Y1 = np.cos(2*np.pi*1*X)
plt.plot(X,Y1)
y2=np.sin(2*np.pi*1*X)
plt.plot(X,Y1,'b',X,y2,'r')


#part tow
print(np.inner(Y1,y2))          #option 1
print(sum(map(mul,Y1,y2)))      #option tow
